#ifdef __OBJC__
#import <Cocoa/Cocoa.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif


FOUNDATION_EXPORT double Pods_EncryptedZipMaker_EncryptedZipMakerUITestsVersionNumber;
FOUNDATION_EXPORT const unsigned char Pods_EncryptedZipMaker_EncryptedZipMakerUITestsVersionString[];

