# Encrypted Zip Maker - OSX

![EZM](/uploads/bd44667d5969d71737209d72df7e9dc3/EZM.png)

*A user friendly utility for creating encrypted ZIP files with ease.*

## About
When sending confidential data we would often like to add an extra layer of security. We mostly rely on zipping tools with complicated user interfaces, third party services or encryption tools which require a special decryptor. Encrypted Zip Maker changes everything by delivering an easy to use interface and generating encrypted ZIP files that can be extracted with OSX own "Archive Utility".

Encrypted Zip Maker - OSX is written as a first Swift project and schould be easily ported to iOS.

## Usage
1. Add input file/folder(s).
2. Choose a password or generate one.
3. Encrypt!

## Requirements

* Mac OSX 10.11+

## Todo
* Create a similar application for both Linux and Windows.

## Building
1. Clone the repository
2. `cd` into the repository and execute `pod install`.
3. Open EncryptedZipMaker.xcworkspace in XCode.  
`CMD + r`  = Build and run.  
`CMD + b`  = Build only.  
`CMD + u`  = Run tests.

## Precompiled binary  
The latest precompiled binary can be downloaded from [releases](https://gitlab.com/jur_bruinink/encrypted-zip-maker-osx/-/releases).  

----
Powered by [SSZipArchive](https://github.com/ZipArchive/ZipArchive)
