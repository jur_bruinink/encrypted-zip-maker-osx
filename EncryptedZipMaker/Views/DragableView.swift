//
//  DragableView.swift
//  EncryptedZipMaker
//
//  Created by Jur on 21/07/2020.
//  Copyright © 2020 Jur. All rights reserved.
//
import Cocoa


// Delegate protocol
@objc protocol DragableViewDelegate
{
    func dragReceived(_ results: [URL])
}


class DragableView: NSView
{

    @IBOutlet weak var delegate: DragableViewDelegate?

    required init?(coder: NSCoder)
    {
        super.init(coder: coder)
        util.filePrint("Intializing!");
        self.drawStatus(false);
        registerForDraggedTypes([.bcFileURL])
    }
    
    // Methods required for dragging
    override func draggingEntered(_ draggingInfo: NSDraggingInfo) -> NSDragOperation
    {
        self.drawStatus(true);
        return .copy;
    }

    override func performDragOperation(_ draggingInfo: NSDraggingInfo) -> Bool
    {
        var fileURLs: [URL] = []
        draggingInfo.draggingPasteboard.readObjects(forClasses: [NSURL.self], options: nil)?.forEach
        {
            eachObject in
                fileURLs.append((eachObject as? URL)!);
        }

        guard fileURLs.count > 0
        else { return false; }

        delegate?.dragReceived(fileURLs);
        return true;
    }

    override func draggingExited(_ sender: NSDraggingInfo?)
    { self.drawStatus(false); }

    override func draggingEnded(_ sender: NSDraggingInfo)
    { self.drawStatus(false); }
    
    func drawStatus(_ dragging:Bool)
    {
        // Draws an overlay and border when a file is being dragged.
        self.wantsLayer = true;
        if (dragging == false)
        {
            self.layer?.backgroundColor = .clear;
            self.layer?.borderWidth = 0;
            self.layer?.borderColor = .clear;
        }
        else
        {
            self.layer?.backgroundColor = .init(red: 0.9, green: 0.9, blue: 0.9, alpha: 0.02);
            self.layer?.borderWidth = 4;
            self.layer?.borderColor = .init(red: 0.1, green: 0.1, blue: 1.0, alpha: 0.3);
        }
    }
}

extension NSPasteboard.PasteboardType
{
    // Backwards compatible fileURL (fileURL is 10.13+).
    static let bcFileURL: NSPasteboard.PasteboardType =
    {

            if #available(OSX 10.13, *) {
                return NSPasteboard.PasteboardType.fileURL;
            } else {
                return NSPasteboard.PasteboardType(kUTTypeFileURL as String);
            }

    } ()
}
