//
//  ListItemModel.swift
//  EncryptedZipMaker
//
//  Created by Jur on 20/07/2020.
//  Copyright © 2020 Jur. All rights reserved.
//

import Cocoa

class ListItem: NSObject
{
    let name:String;
    var icon:NSImage?;
    var path:String;
    var folder:Bool;
    
    init (_ name:String, _ path:String, _ is_folder:Bool)
    {
        self.name   = name;
        self.folder = is_folder
        self.icon   = self.folder ? NSImage (named: "GenericFolderIcon") : NSImage (named: "GenericDocumentIcon");
        self.path   = path;
    }
}

