//
//  Utilities.swift
//  EncryptedZipMaker
//
//  Created by Jur on 21/07/2020.
//  Copyright © 2020 Jur. All rights reserved.
//

import Foundation

class util
{
    class func filePrint(_ msg:String, file_path:String = #file)
    {
        // Prepends print messages with [filename]. Retrieves the path of the
        // calling file (/some/folder/Class.swift), gets the last component (Class.swift)
        // splits the file at the extension and prints the first item (Class).
        let file = URL(fileURLWithPath: file_path).lastPathComponent;
        print("[" + file.split(separator: ".")[0] + "] " + msg);
    }
}
