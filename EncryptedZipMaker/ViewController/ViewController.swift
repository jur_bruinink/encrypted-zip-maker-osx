//
//  ViewController.swift
//  EncryptedZipMaker
//
//  Created by Jur on 09/07/2020.
//  Copyright © 2020 Jur. All rights reserved.
//

import Cocoa;

class ViewController: NSViewController, NSOutlineViewDelegate, NSOutlineViewDataSource, DragableViewDelegate
{
    
    // Class object placeholders
    var infoTimer:Timer?                 = nil;
    var zipThread:Thread?                = nil;
    var threadTimer:Timer?               = nil;
    var zipManager:ZipManager?           = nil;
    var listManager:UIListManager?       = nil;
    var fsManager:FilesystemManager?     = nil;
    var securityManager:SecurityManager? = nil;
    
    // Class variables
    var zip_success              = false;
    var is_processing            = false;
    var output_file:URL          = URL(fileURLWithPath: "");
    var picked_items:[URL]       = [];
    var password                 = "";
    var show_password            = false;

    // Class constants
    let temp_folder   = "EZMTempStorage";
    
    // UI objects
    @IBOutlet weak var hiddenPassField: NSSecureTextField!
    @IBOutlet weak var plainPassField: NSTextField!
    @IBOutlet weak var progressIndicator: NSProgressIndicator!
    @IBOutlet weak var infoField: NSTextField!
    @IBOutlet weak var listView: NSOutlineView!
    @IBOutlet weak var addItemButton: NSButton!
    @IBOutlet weak var remItemButton: NSButton!
    @IBOutlet weak var tutorialView: NSView!
    
    // Class methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Create instances
        util.filePrint("Creating class instances.");
        self.fsManager       = FilesystemManager();
        self.zipManager      = ZipManager();
        self.listManager     = UIListManager();
        self.securityManager = SecurityManager(self.fsManager!);
        // Hide plaintext password field.
        self.plainPassField.isHidden = false;
        // Make tutorial visible and fade out.
        self.fadeTutorial(2.5, 5.0);
    }

    override var representedObject: Any?
    {
        didSet
        {
        // Update the view, if already loaded.
        }
    }
    
    func fadeTutorial(_ after:Double, _ fade_time:Double)
    {
        // Fade out on the tutorial window.
        self.tutorialView.alphaValue = 1.0;
        // Keep visible for x seconds, then start fade out.
        DispatchQueue.main.asyncAfter(deadline: .now() + after) {
            if #available(OSX 10.12, *)
            {
                // OSX 10.12+ animation
                NSAnimationContext.runAnimationGroup({context in
                    context.duration = fade_time;
                    self.tutorialView.animator().alphaValue = 0;
                })
            }
            else
            {
                // Fallback for 10.11
                NSAnimationContext.beginGrouping();
                NSAnimationContext.current.duration = fade_time;
                self.tutorialView.animator().alphaValue = 0;
                NSAnimationContext.endGrouping();
            }
        }
        
    }
    
    func showInfo(_ msg:String)
    {
        // Shows information in the infoField and starts a timer.
        // When the timer runs out clearInfo() is called clearing
        // the message from the screen.
        self.infoField.stringValue = msg;
        self.infoTimer?.invalidate();
        self.infoTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self,
                                              selector: #selector(self.clearInfo),
                                              userInfo: nil, repeats: false);
    }
    
    func showInfoPersist(_ msg:String)
    {
        // Shows information in the infoField persistently until
        // a new message is set.
        self.infoTimer?.invalidate();
        self.infoField.stringValue = msg;
    }
    
    
    @objc func clearInfo()
    {
        // Gets called by the infoTimer ( started by showInfo() ).
        // Clears the infoField from any previous message.
        self.infoTimer?.invalidate();
        self.infoField.stringValue = "";
    }
    
    func createZip()
    {
        // Gets called when the user clicks the encrypt button and all requirements are met.
        // Retrieves the temporary folder, copies all selected files to it and starts the creation
        // of the ZIP in a new thread to stop it from blocking the UI.
        self.is_processing = true;
        self.showInfoPersist("Creating temp folder.");
        if (self.fsManager!.createFolder(self.temp_folder))
        {
            self.showInfoPersist("Copying data to temp dir..");
            if(self.fsManager!.copyDataToFolder(self.picked_items, self.temp_folder))
            {
                self.showInfoPersist("Creating encrypted ZIP file.");
                self.threadTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self,
                                                         selector: #selector(self.zipThreadFinished),
                                                         userInfo: nil, repeats: true);
                Thread.detachNewThreadSelector(#selector(self.zipCreateThread), toTarget: self, with: nil);
                
            }
            else
            {
                util.filePrint("Could not copy files to temp dir.");
            }
        }
        else
        {
            util.filePrint("Could not create temp folder.");
        }
    }
    
    @objc func zipCreateThread()
    {
        // Creates the ZIP file. This function is spawned in a new thread
        // to stop the UI from freezing.
        self.zip_success = self.zipManager!.createZip(self.output_file,
                                                      self.fsManager!.folderNameToUrl(self.temp_folder),
                                                      self.password);
        self.is_processing = false;
    }
    
    @objc func zipThreadFinished()
    {
        // Get's called by a repeating timer to check if zipThread is done working.
        if !(self.is_processing)
        {
            // Stop timer, remove temp files and tell the user if everything went OK.
            self.threadTimer!.invalidate();
            self.showInfo("Removing temp dir..");
            if !(self.fsManager!.remFolder(self.temp_folder))
            {
                util.filePrint("Could not remove temp folder!");
            }
            if (self.zip_success == true)
            {
                self.showInfo("ZIP file created successfully!");
                util.filePrint("ZIP creation and cleanup successfull!");
            }
            else
            {
                self.showInfo("Failed to create ZIP file!");
            }
            self.progressIndicator.stopAnimation(self);
        }
    }
    
    func outlineView(_ outlineView: NSOutlineView, numberOfChildrenOfItem item: Any?) -> Int
    {
        return self.listManager?.getCount() ?? 0;
    }

    func outlineView(_ outlineView: NSOutlineView, child index: Int, ofItem item: Any?) -> Any
    {
        return self.listManager!.getItem(index);
    }
    
    func outlineView(_ outlineView: NSOutlineView, isItemExpandable item: Any) -> Bool
    {
        return false;
    }
    
    func outlineView(_ outlineView: NSOutlineView, viewFor tableColumn: NSTableColumn?, item: Any) -> NSView?
    {
      var view: NSTableCellView?
      if let list_item = item as? ListItem
      {
        view = outlineView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "DataCell"), owner: self) as? NSTableCellView
        view!.textField!.stringValue = list_item.name;
        view!.imageView!.image       = list_item.icon;
      }
      return view
    }
    
    func selectOutputFile()
    {
        // Executes when a user clicks the select button for the output file.
        // Opens a filepicker to make the user select an output file name and location.
        let window = view.window;
        if !(self.is_processing)
        {
            let savePanel = NSSavePanel();
            savePanel.nameFieldStringValue = "output.zip";
            savePanel.allowedFileTypes     = ["zip"];
            savePanel.canCreateDirectories = true;
            savePanel.showsTagField        = false;
            savePanel.level = NSWindow.Level(rawValue: Int(CGWindowLevelForKey(.modalPanelWindow)));
            savePanel.beginSheetModal(for: window!) { (result) in
                if result.rawValue == NSApplication.ModalResponse.OK.rawValue
                {
                    self.output_file = savePanel.url!;
                    self.createZip();
                }
                else
                {
                    // No file was selected.
                    self.is_processing = false;
                    self.progressIndicator.stopAnimation(self);
                }
            }
        }
    }
    
    func showPassword()
    {
        // Executes when the user clicks the show password button.
        // Shows the password by changing between a secure and
        // a normal textfield.
        self.plainPassField.stringValue = self.hiddenPassField.stringValue;
        self.hiddenPassField.isHidden = true;
        self.plainPassField.isHidden = false;
        self.show_password = true;
    }
    
    @objc func hidePassword()
    {
        self.hiddenPassField.stringValue = self.plainPassField.stringValue;
        self.hiddenPassField.isHidden = false;
        self.plainPassField.isHidden = true;
        self.show_password = false;
    }
    
    
    
    // UI functions
    
    func dragReceived(_ results: [URL]) {
        // Is called when a user drags a file onto the view.
        util.filePrint("User dragged: \(results)");
        for result in results
        {
            // Add files to be listed.
            self.listManager?.addItem(result.lastPathComponent, result.path, result.hasDirectoryPath);
        }
        self.listView.reloadData();
        
    }
    
    @IBAction func showPassButtonClicked(_ sender: Any)
    {
        if(self.show_password == false)
        {
            self.showPassword();
            return;
        }
        self.hidePassword();
    }
    
    @IBAction func createButtonClicked(_ sender: Any)
    {
        // Executes when the user clicks the encrypt button.
        // Checks if all requirements are met and either executes createZIP
        // or return the unmet requirement in the UI.
        if !(self.is_processing)
        {
            util.filePrint("Create clicked!");
            self.progressIndicator.startAnimation(self);
            if (self.show_password == false)
            {
                self.password = self.hiddenPassField.stringValue;
            }
            else
            {
                self.password = self.plainPassField.stringValue;
            }
            self.picked_items = self.listManager!.getItemURLS();
            // Requirement checking in the same order as they appear in the UI.
            if !((self.picked_items.count) > 0)
            {
                self.showInfo("Please select at least one input file.");
                self.progressIndicator.stopAnimation(self);
                return;
            }
            if !(self.password.count > 0 )
            {
                self.showInfo("Please enter a password.");
                self.progressIndicator.stopAnimation(self);
                return;
            }
            // Open a window for the user to select an output file.
            // Calls createZip() when a valid output location is selected.
            self.selectOutputFile();
        }
    }
    
    @IBAction func generateButtonClicked(_ sender: Any)
    {
        // Executes when the user clicks the generate button.
        // Generates a random password and copies it to the clipboard.
        if (!(self.securityManager == nil) && !(self.is_processing))
        {
            // Hardcoded password length for now
            let password = self.securityManager!.generatePassword(16);
            if (self.show_password == false)
            {
                self.hiddenPassField.stringValue = password;
            }
            else
            {
                self.plainPassField.stringValue = password;
            }
            self.securityManager!.copyPasswordToClipboard(password);
            self.showInfo("Copied password to clipboard!");
        }
    }

    @IBAction func addItemButtonClicked(_ sender: Any)
    {
        // Executes when a user clicks the + button for input items.
        // Opens a filepicker to make the user select (an) input item(s).
        let window = view.window;
        if !(self.is_processing)
        {
            util.filePrint("Opening file dialog.");
            //guard let window = view.window else { return };
            let openPanel = NSOpenPanel();
            
            openPanel.title                   = "Select your input file/folder(s)";
            openPanel.showsResizeIndicator    = true;
            openPanel.showsHiddenFiles        = false;
            openPanel.canChooseDirectories    = true;
            openPanel.canCreateDirectories    = true;
            openPanel.allowsMultipleSelection = true;
            openPanel.beginSheetModal(for: window!) { (result) in
                if result.rawValue == NSApplication.ModalResponse.OK.rawValue
                {
                    let results = openPanel.urls;
                    if (results.count > 0)
                    {
                        for result in results
                        {
                            // Add files to be listed.
                            self.listManager?.addItem(result.lastPathComponent, result.path, result.hasDirectoryPath)
                        }
                        self.listView.reloadData();
                    }
                }
                else
                {
                    // User clicked on "Cancel"
                    return
                }
            }
        }
    }
    
    @IBAction func remItemButtonClicked(_ sender: Any)
    {
        // Executes when a user clicks the - button.
        // removes the selected input item from the list.
        let selected_item = self.listView.selectedRow;
        if (selected_item != -1)
        {
            self.listManager!.removeItem(selected_item);
            self.listView.reloadData();
        }
    }
    
    
    @IBAction func remAllItemsClicked(_ sender: Any)
    {
        // Executes when a user clicks the x button.
        // removes all input items from the list.
        self.listManager!.removeAll();
        self.listView.reloadData();
    }
    
}

