//
//  FilesystemManager.swift
//  EncryptedZipMaker
//
//  Created by Jur on 13/07/2020.
//  Copyright © 2020 Jur. All rights reserved.
//

import Foundation
import SwiftUI

class FilesystemManager
{
    
    // Class methods
    init()
    {
        // Class constructor
        util.filePrint("Initializing!");
    }

    func folderNameToUrl(_ folder_name: String) -> URL
    {
        return URL(fileURLWithPath: NSTemporaryDirectory() + folder_name, isDirectory: true);
    }
    
    public func copyDataToFolder(_ files: [URL],_ folder_name: String) -> Bool
    {
        // Takes an array of file URL's and copies them one by one to the specified folder.
        let file_count = files.count;
        util.filePrint("Copying \(file_count) file(s)");
        for n in 0...(file_count - 1)
        {
            if(self.copyToFolder(files[n], self.folderNameToUrl(folder_name)) == nil)
            {
                util.filePrint("Failed to copy \(files[n].absoluteString)");
                return false;
            }
        }
        return true;
    }
    
    private func copyToFolder(_ source_url: URL,_ destination_url: URL) -> URL?
    {
        // Copies a single file/folder to a specified folder.
        let target_url = URL(fileURLWithPath: (destination_url.path + "/" + source_url.lastPathComponent));
        util.filePrint("Copying \(source_url.lastPathComponent) to \(target_url.path).");
        do
        {
            try FileManager.default.copyItem(at: source_url, to: target_url);
            return target_url as URL;
        }catch let error
        {
            util.filePrint("Unable to copy file: \(error)");
        }
        return nil
    }
    
    public func createFile(_ file_path: URL,_ file_data: Data) -> Bool
    {
        // Creates a file containing the supplied data at the supplied path.
        util.filePrint("Creating file \(file_path.lastPathComponent)");
        return FileManager.default.createFile(atPath: file_path.path, contents: file_data, attributes: nil);
    }
    
    public func remFile(_ file_path: URL) -> Bool
    {
        // Removes file at given path
        util.filePrint("Removing file \(file_path.lastPathComponent)");
        do
        {
            try FileManager.default.removeItem(at: file_path);
            return true;
        }
        catch let error
        {
            util.filePrint("Failed to remove file: \(error)");
            return false;
        }
    }
    
    public func createFolder(_ folder_name: String) -> Bool
    {
        // Creates a folder with the supplied name.
        util.filePrint("Creating folder \(folder_name)");
        do
        {
            try FileManager.default.createDirectory(at: self.folderNameToUrl(folder_name),
                                                    withIntermediateDirectories: true,
                                                    attributes: nil);
            return true;
        }
        catch let error
        {
            util.filePrint("Unable create folder: \(error)");
            return false;
        }
    }
    
    public func remFolder(_ folder_name: String) -> Bool
    {
        // Removes the specified folder.
        util.filePrint("Removing folder \(folder_name)");
        do
        {
            try FileManager.default.removeItem(atPath: self.folderNameToUrl(folder_name).path);
            return true;
        }catch let error
        {
            util.filePrint("Failed to remove folder: \(error)");
            return false;
        }
    }
    
    public func fileExists(_ file: URL) -> Bool
    {
        // Returns true if file exists.
        return FileManager.default.fileExists(atPath: file.path);
    }
    
    public func folderExists(_ folder_name: String) -> Bool
    {
        // Returns true if file exists and is a folder.
        var is_directory : ObjCBool = false;
        return (FileManager.default.fileExists(atPath: self.folderNameToUrl(folder_name).path,
                                               isDirectory: &is_directory) && is_directory.boolValue);
    }
}
