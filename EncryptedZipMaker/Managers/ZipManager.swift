//
//  ZipManager.swift
//  EncryptedZipMaker
//
//  Created by Jur on 12/07/2020.
//  Copyright © 2020 Jur. All rights reserved.
//

import Foundation
import SwiftUI
import SSZipArchive

class ZipManager
{    
    // Class methods
    init()
    {
        // Constructor
        util.filePrint("Initializing!");
    }
    
    func createZip(_ zip_file:URL,_ input_folder:URL,_ password:String) -> Bool
    {
        // Calls the SSZipArchive library to create an encrypted ZIP file.
        util.filePrint(("Creating " + zip_file.lastPathComponent + " with password " + password));
        return SSZipArchive.createZipFile(atPath: zip_file.path, withContentsOfDirectory: input_folder.path,
                                          keepParentDirectory: false, compressionLevel: 1, password: password,
                                          aes: false, progressHandler: nil);
    }
    
    func extractZip(_ zip_file:URL,_ dest_folder: URL,_ password:String) -> Bool
    {
        util.filePrint(("Extracting " + zip_file.lastPathComponent + " to " + dest_folder.path));
        do
        {
            try SSZipArchive.unzipFile(atPath: zip_file.path, toDestination: dest_folder.path,
                                       overwrite: true, password: password);
            return true;
        }
        catch let error
        {
            util.filePrint("Could not extract zip : \(error).");
            return false;
        }
    }
}
