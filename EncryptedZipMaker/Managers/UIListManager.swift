//
//  UIListManager.swift
//  EncryptedZipMaker
//
//  Created by Jur on 20/07/2020.
//  Copyright © 2020 Jur. All rights reserved.
//

import Foundation

class UIListManager
{
    // Class constants
    var item_list:[ListItem] = [];
    
    init()
    {
        util.filePrint("Initializing!");
    }
    
    func addItem(_ item_name:String, _ path:String, _ item_is_folder:Bool)
    {
        // Adds an item to the list if it doesn't exist yet.
        
        // Cycle through current files.
        if (self.item_list.count > 0)
        {
            for i in 0...(self.item_list.count - 1)
            {
                if (self.item_list[i].path == path)
                {
                    // Do not add the file if it was already added.
                    return;
                }
            }
        }
        self.item_list.append(ListItem(item_name, path, item_is_folder));
    }
    
    func getItem(_ item:Int) -> AnyObject
    {
        // Returns an item.
        return self.item_list[item];
    }
    
    func getItemURLS() -> [URL]
    {
        // Returns all items.
        var item_urls:[URL] = [];
        if (self.item_list.count > 0)
        {
            for i in 0...(self.item_list.count - 1)
            {
                item_urls.append(URL(fileURLWithPath:self.item_list[i].path, isDirectory: self.item_list[i].folder));

            }
        }
        return item_urls;
    }
    
    func removeItem(_ item:Int)
    {
        // Removes an item.
        self.item_list.remove(at: item);
    }
    
    func removeAll()
    {
        self.item_list.removeAll();
    }
    
    func getCount() -> Int
    {
        // Returns the amount of items in the list.
        return item_list.count;
    }
}
