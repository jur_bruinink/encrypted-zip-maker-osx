//
//  SecurityManager.swift
//  EncryptedZipMaker
//
//  Created by Jur on 13/07/2020.
//  Copyright © 2020 Jur. All rights reserved.
//

import Foundation
import SwiftUI

class SecurityManager
{
    
    // Class object placeholders
    var fsManager:FilesystemManager? = nil;
    
    // Class methods
    init(_ fsMan: FilesystemManager)
    {
        // Constructor
        util.filePrint("Initializing!");
        self.fsManager = fsMan;
        self.cleanTempFolder("EZMTempStorage");
    }
    
    func generatePassword(_ length: Int) -> String {
        // Generates a random password length x using the specified alphabet.
        let alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_+=|{}[];:,<.>?~";
        return String((0..<length).map{ _ in alphabet.randomElement()! });
    }
    
    func copyPasswordToClipboard(_ password: String)
    {
        // Copies the provided password to the clipboard
        NSPasteboard.general.clearContents();
        NSPasteboard.general.setString(password, forType: .string);
    }
    
    func cleanTempFolder(_ folder_name: String)
    {
        // Checks if temporary folder remained on disk because of a crash or
        // user exit and removes it. Gets called at program startup.
        util.filePrint("Checking for uncleared temp data.");
        if(fsManager!.folderExists(folder_name))
        {
            util.filePrint("Found old temp folder!");
            if !(fsManager!.remFolder(folder_name))
            {
                util.filePrint("Could not remove temp folder!");
            }
        }
    }
}
