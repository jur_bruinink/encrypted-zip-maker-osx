//
//  SecurityManagerTests.swift
//  EncryptedZipMakerTests
//
//  Created by Jur on 15/07/2020.
//  Copyright © 2020 Jur. All rights reserved.
//

import XCTest
@testable import EncryptedZipMaker

class SecurityManagerTests: XCTestCase
{

    var securityManager:SecurityManager? = nil;
    var fsManager:FilesystemManager?     = nil;
    
    override func setUpWithError() throws
    {
        self.fsManager = FilesystemManager();
        self.securityManager = SecurityManager(self.fsManager!);
        XCTAssertNotNil(self.fsManager, "fsManager should not be nil after init.");
        XCTAssertNotNil(self.securityManager, "securityManager should not be nil after init.");
    }

    override func tearDownWithError() throws
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testGeneratePasswordLength() throws
    {
        // This tests uses the SecurityManager to generate a password of random length and then checks if the
        // generated password matches this lenght.
        //
        let password_length = Int.random(in: 1..<64);
        let password = self.securityManager!.generatePassword(password_length);
        XCTAssertEqual(password.count, password_length, "Generated password length should equal input length.");
    }
    
    func testCopyPasswordToClipboard() throws
    {
        // Tests if copying passwords to clipboard works as expected by generating a password and calling copyPasswordToClipboard()
        // after that the current clipboard contents are checked against the generated password.
        //
        let password = self.securityManager!.generatePassword(16);
        self.securityManager!.copyPasswordToClipboard(password);
        XCTAssertEqual(NSPasteboard.general.string(forType: .string), password, "Clipboard should contain the generated password.")
    }
    
    func testCleanTempFolder() throws
    {
        // Tests if a leftover temp folder is indeed discovered and removed.
        //
        XCTAssertTrue(self.fsManager!.createFolder("TestFolderCleanup"), "Creating the folder should succeed.");
        self.securityManager!.cleanTempFolder("TestFolderCleanup");
        XCTAssertFalse(self.fsManager!.folderExists("TestFolderCleanup"), "Folder should not exist after removal");
    }
}
