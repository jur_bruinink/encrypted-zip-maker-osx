//
//  FilesystemManagerTests.swift
//  EncryptedZipMakerTests
//
//  Created by Jur on 15/07/2020.
//  Copyright © 2020 Jur. All rights reserved.
//

import XCTest
@testable import EncryptedZipMaker

class FilesystemManagerTests: XCTestCase {

    let temp_folder    = "EZMTestFolder";
    let temp_file_data = Data(base64Encoded: "dGVzdGZpbGU="); // Plain: testfile
    var temp_file_path = URL(fileURLWithPath: "");
    var fsManager:FilesystemManager? = nil;
    
    override func setUpWithError() throws
    {
        self.fsManager = FilesystemManager();
        XCTAssertNotNil(self.fsManager, "fsManager should not be nil after init.");
        self.temp_file_path = URL(fileURLWithPath: self.fsManager!.folderNameToUrl(self.temp_folder).path + "/testfile.txt");
    }

    override func tearDownWithError() throws
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testCreateFolder() throws
    {
        // Tests if a temporary folder can be created. Checks if it exists after and finally removes it.
        //
        XCTAssertTrue(self.fsManager!.createFolder(self.temp_folder), "Creating a folder should succeed.");
        XCTAssertTrue(self.fsManager!.folderExists(self.temp_folder), "Folder should exist after creation.");
        // Cleanup
        XCTAssertTrue(self.fsManager!.remFolder(self.temp_folder), "Removal of a folder should succeed.");
        
    }
    
    func testRemFolder() throws
    {
        // Creates a temp directory, removes it and checks if it is actually removed.
        //
        XCTAssertTrue(self.fsManager!.createFolder(self.temp_folder), "Creating a folder should succeed.");
        XCTAssertTrue(self.fsManager!.remFolder(self.temp_folder), "Removal of a folder should succeed.");
        XCTAssertFalse(self.fsManager!.folderExists(self.temp_folder), "Temporary folder should not exist after removal.");
    }
    
    func testCreateFile() throws
    {
        // Creates a temp directory containing a file and checks if the file exists.
        //
        XCTAssertTrue(self.fsManager!.createFolder(self.temp_folder), "Creating a folder should succeed.");
        XCTAssertTrue(self.fsManager!.createFile(self.temp_file_path, self.temp_file_data!), "Creating a file should succeed.");
        XCTAssertTrue(self.fsManager!.fileExists(self.temp_file_path), "File should exist after creation.");
        // Cleanup
        XCTAssertTrue(self.fsManager!.remFolder(self.temp_folder), "Removal of a folder should succeed.");
        
    }
    
    func testRemFile() throws
    {
        // Creates a temp directory containing a file , removes the file and checks if it is actually removed.
        //
        XCTAssertTrue(self.fsManager!.createFolder("EZMTestFolder"), "Creating a folder should succeed.");
        XCTAssertTrue(self.fsManager!.createFile(self.temp_file_path, self.temp_file_data!), "Creating a file should succeed.");
        XCTAssertTrue(self.fsManager!.fileExists(self.temp_file_path), "File should exist after creation.");
        XCTAssertTrue(self.fsManager!.remFile(self.temp_file_path), "File removal should succeed.");
        XCTAssertFalse(self.fsManager!.fileExists(self.temp_file_path), "File should not exist after removal.");
        // Cleanup
        XCTAssertTrue(self.fsManager!.remFolder(self.temp_folder), "Removal of a folder should succeed.");
    }
    
    func testCopyFileToFolder() throws
    {
        // Creates a test file, copies it to a nested folder checks if the move was successfull and cleans up after.
        //
        let nested_folder  = self.temp_folder + "/EZMNestedFolder";

        XCTAssertTrue(self.fsManager!.createFolder(self.temp_folder),  "Creating a folder should succeed.");
        XCTAssertTrue(self.fsManager!.createFolder(nested_folder),"Creating a folder should succeed.");
        XCTAssertTrue(self.fsManager!.createFile(self.temp_file_path, self.temp_file_data!), "Creating a file should succeed.");
        XCTAssertTrue(self.fsManager!.copyDataToFolder([self.temp_file_path.absoluteURL], nested_folder), "Copying a file should succeed.");
        
        // Cleanup
        XCTAssertTrue(self.fsManager!.remFolder(nested_folder), "Removal of a folder should succeed.");
    }

}
