//
//  ZipManagerTests.swift
//  EncryptedZipMakerTests
//
//  Created by Jur on 15/07/2020.
//  Copyright © 2020 Jur. All rights reserved.
//

import XCTest
@testable import EncryptedZipMaker

class ZipManagerTests: XCTestCase
{
    let password        = "TestPass";
    let test_file_data  = Data(base64Encoded: "dGVzdGZpbGU="); // Plain: testfile
    var zipManager:ZipManager?       = nil;
    var fsManager:FilesystemManager? = nil;
    
    override func setUpWithError() throws
    {
        self.zipManager = ZipManager();
        self.fsManager = FilesystemManager();
        XCTAssertNotNil(self.zipManager, "zipManager should not be nil after init.");
        XCTAssertNotNil(self.fsManager, "fsManager should not be nil after init.");
    }

    override func tearDownWithError() throws
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testZipUnzip() throws
    {
        // Creates a ZIP and then extracts it. The contents of the pre- and post archived file should match.
        //
        let in_folder_url    = self.fsManager!.folderNameToUrl("EZMTestFolder/in/");
        let out_folder_url   = self.fsManager!.folderNameToUrl("EZMTestFolder/out/");
        let zip_folder_url   = self.fsManager!.folderNameToUrl("EZMTestFolder/zip/");
        let zip_url          = URL(fileURLWithPath: zip_folder_url.path + "/temp_zip.zip");
        let file_url         = URL(fileURLWithPath: in_folder_url.path + "/testfile.txt");
        let file_out_url     = URL(fileURLWithPath: out_folder_url.path + "/testfile.txt");
        
        // Create in,out & zip folders and a file to zip.
        XCTAssertTrue(self.fsManager!.createFolder("EZMTestFolder/in"),  "Creating a folder should succeed.");
        XCTAssertTrue(self.fsManager!.createFolder("EZMTestFolder/out"), "Creating a folder should succeed.");
        XCTAssertTrue(self.fsManager!.createFolder("EZMTestFolder/zip"), "Creating a folder should succeed.");
        XCTAssertTrue(self.fsManager!.createFile(file_url, self.test_file_data!),   "Creating a file should succeed.");
        
        // Zip and unzip and check if the unzipped file exists.
        XCTAssertTrue(self.zipManager!.createZip(zip_url, in_folder_url, self.password),   "Creating a ZIP file should succeed.");
        XCTAssertTrue(self.zipManager!.extractZip(zip_url, out_folder_url, self.password), "Extracting a ZIP file should succeed.");
        XCTAssertTrue(self.fsManager!.fileExists(file_out_url), "Testfile should exist after extraction.");
        
        // Decode input file data, read output file contents.
        let plain_input  = String(decoding: self.test_file_data!, as: UTF8.self);
        let plain_output = try String(contentsOf: file_out_url);
        
        // Check if file contents are equal. TODO: Check file hash instead of content and increase content size.
        XCTAssertEqual(plain_input, plain_output, "File contents should be equal pre- & post ZIP.");
        
        // Cleanup
        XCTAssertTrue(self.fsManager!.remFolder("EZMTestFolder"), "Removing a folder should succeed.");
        
    }

}
